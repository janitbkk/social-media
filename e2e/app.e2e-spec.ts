import { SocialMediaPage } from './app.po';

describe('social-media App', () => {
  let page: SocialMediaPage;

  beforeEach(() => {
    page = new SocialMediaPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
