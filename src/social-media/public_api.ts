export { SocialMediaModule } from './social-media.module';
export { SocialLinks, SocialMediaConfig, SocialLinkType } from './social-media-config';
