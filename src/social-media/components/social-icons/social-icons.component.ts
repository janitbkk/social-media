import { Component, Input, Inject, ChangeDetectionStrategy } from '@angular/core';
import { SOCIAL_MEDIA_CONFIG, SocialMediaConfig } from '../../social-media-config';

export type LinkShapes = 'square' | 'round' | 'circle' | 'none';

@Component({
  selector: 'casdl-social-icons',
  templateUrl: './social-icons.component.html',
  styleUrls: ['./social-icons.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SocialIconsComponent {
  @Input() shape: LinkShapes = 'square';
  @Input() colored = true;

  public links = Object.keys(this.socialMediaConfig.links)
    .map(link => ({ type: link, ...this.socialMediaConfig.links[link] }));

  constructor(
    @Inject(SOCIAL_MEDIA_CONFIG) private socialMediaConfig: SocialMediaConfig
  ) {}

  linkClass(type: string) {
    return `social-icon-${type}`;
  }

  iconClass(type: string) {
    return `fa-${type}`;
  }

}
