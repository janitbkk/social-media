import { InjectionToken } from '@angular/core';

export const SOCIAL_MEDIA_CONFIG = new InjectionToken('social media config');

export type SocialLinkType =
  | 'facebook'
  | 'twitter'
  | 'linkedin'
  | 'reddit'
  | 'tumblr'
  | 'instagram'
  | 'google-plus'
  | 'pinterest'
  | 'vk'
  | 'tripadvisor';

export type SocialLinks = {
  [T in SocialLinkType]?: { url: string };
};

export interface SocialMediaConfig {
  links: SocialLinks;
}
