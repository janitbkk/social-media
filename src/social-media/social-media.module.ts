import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SocialMediaConfig, SOCIAL_MEDIA_CONFIG } from './social-media-config';
import { SocialIconsComponent } from './components/social-icons/social-icons.component';

@NgModule({
  imports: [CommonModule],
  exports: [SocialIconsComponent],
  declarations: [SocialIconsComponent]
})
export class SocialMediaModule {

  static forRoot(socialMediaConfig: SocialMediaConfig): ModuleWithProviders {
    return {
      ngModule: SocialMediaModule,
      providers: [
        { provide: SOCIAL_MEDIA_CONFIG, useValue: socialMediaConfig },
      ]
    };
  }
}

